.PHONY: default

TERRAFORM=docker run --rm -t -v $(PWD):/app -w /app -e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) -e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) hashicorp/terraform:0.12.9

default: init plan
init:
	@$(TERRAFORM) init
plan:
	@$(TERRAFORM) plan
apply:
	@$(TERRAFORM) apply --auto-approve
destroy:
	@$(TERRAFORM) destroy